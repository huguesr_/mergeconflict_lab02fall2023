package application;
//2242264 Hugues Rouillard
import vehicles.Bicycle;
public class BikeStore {
    
    public static void main(String[] args){
        Bicycle[] bicycleArray = new Bicycle[4];
        bicycleArray[0] = new Bicycle("Specialized", 4, 50.4);
        bicycleArray[1] = new Bicycle("Kona", 13, 240);
        bicycleArray[2] = new Bicycle("Louis-Garneau", 20, 10.23);
        bicycleArray[3] = new Bicycle("Btwin", 10, 37.9);

        for(int i = 0; i<bicycleArray.length; i++){
            System.out.println(bicycleArray[i]);
        }

    }
}
